@extends('layouts.app')

@section('content')
@if(Auth::user()->role == 'Admin')
    <div class="page-header">
        <h1 class="page-title">Pembayaran SPP</h1>
    </div>
@endif
    <div class="row">
        @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Petugas' )
        <div class="col-12">
            <div class="card">
                <form action="{{ route('spp.store') }}" method="post">
                <div class="card-header">
                    <h3 class="card-title">Transaksi</h3>
                </div>
                    <div class="card-body">
                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                    @endif
                        <div class="col-12">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="{{ Auth::user()->nign }}" required>
                                <label class="form-label">NIS</label>
                                <input type="number" class="form-control" name="nis" placeholder="Masukkan NIS siswa" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Bulan</label>
                                <select id="select-beast" class="form-control custom-select" name="bulan">
                                    @foreach($bulan as $b)
                                        <option value="{{ $b }}">{{ $b }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Histori Transaksi</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-hover table-vcenter text-wrap">
                        <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>Nama siswa</th>
                            <th>Bulan</th>
                            <th>Tanggal bayar</th>
                            <th>Nama petugas</th>
                            @if(Auth::user()->role == 'Admin')
                            <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        @foreach($transaksi as $no => $t)
                        <tbody>
                            <td><span class="text-muted">{{ ++$no + ($transaksi->currentPage()-1) * $transaksi->perPage() }}</span></td>
                            <td>{{ $t->siswa->nama }}</td>
                            <td>{{ $t->bulan }}</td>
                            <td>{{ $t->created_at->format('d-M-Y') }}</td>
                            <td>{{ $t->petugas->nama_petugas }}</td>
                            @if(Auth::user()->role == 'Admin')
                            <td>
                                <a class="icon" href="transaksi/{{ $t->id }}/ubah" title="edit item">
                                    <i class="fe fe-edit"></i> 
                                </a>
                                <a class="icon btn-delete" href="transaksi/{{ $t->id }}/hapus" data-id="" title="delete item">
                                    <i class="fe fe-trash"></i>
                                </a>
                            </td>
                            @endif
                        </tbody>
                        @endforeach
                    </table>
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        {{ $transaksi->links() }}
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if(Auth::user()->role == 'Siswa')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Histori Transaksi</h3>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-hover table-vcenter text-wrap">
                        <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>Nama siswa</th>
                            <th>Bulan</th>
                            <th>Tanggal bayar</th>
                            <th>Nama petugas</th>
                        </tr>
                        </thead>
                        @foreach($siswa as $no => $s)
                        <tbody>
                            <td><span class="text-muted">{{ ++$no + ($siswa->currentPage()-1) * $siswa->perPage() }}</span></td>
                            <td>{{ $s->siswa->nama }}</td>
                            <td>{{ $s->bulan }}</td>
                            <td>{{ $s->created_at->format('d-M-Y') }}</td>
                            <td>{{ $s->petugas->nama_petugas }}</td>
                        </tbody>
                        @endforeach
                    </table>
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        {{ $siswa->links() }}
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection