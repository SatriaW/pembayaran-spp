@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-8">
            @foreach($transaksi as $t)
            <form action="/transaksi/update" method="post" class="card">
                {{ csrf_field() }}
                <div class="card-header">
                    <h3 class="card-title">Edit transaksi SPP</h3>
                </div>
                <div class="card-body">
                     @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                    @endif 
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label class="form-label">NIGN</label>
                            <input type="number" class="form-control" name="nign" value="{{ $t->nign }}" required>
                            </div>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="{{ $t->id }}" required>
                                <label class="form-label">NIS</label>
                                <input type="number" class="form-control" name="nis" placeholder="Masukkan NIS siswa" value="{{ $t->nis }}" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Bulan</label>
                                <select id="select-beast" class="form-control custom-select" name="bulan">
                                    @foreach($bulan as $b)
                                        <option value="{{ $b }}">{{ $b }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        <a href="{{ url()->previous() }}" class="btn btn-link">Batal</a>
                        <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                    </div>
                </div>
            </form>
            @endforeach
        </div>
    </div>
@endsection