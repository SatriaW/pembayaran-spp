<!doctype html>
<html lang="en" dir="ltr">
<head>
	<meta charset="UTF-8">
    <link rel="icon" type="image/png" href="{{ asset('img/logosmkn4.png') }}">
	<title>Pembayaran SPP</title>
	<link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" />
</head>
<body class="" style="background-color: white;">
    <div class="page">
        <div class="page-single">
            <div class="container">
                <center><img src="{{ asset('img/logosmkn4.png') }}" style="width: 100px;margin-bottom: 50px"></center>
                <div class="row">
                    <div class="col col-login mx-auto">
                        <form class="card" action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="card-body p-6">
                                <div class="card-title"><h3>Masuk</h3></div>
                                <div class="form-group">
                                    <label class="form-label">Email</label>
									<input type="email" name="email" class="form-control {{ ($errors->has('email')) ? 'is-invalid' : '' }}" placeholder="Masukan email" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                        <small class="form-text invalid-feedback" style="display: block !important">{{ $errors->first('email') }}</small>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Password</label>
                                    <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'is-invalid' : '' }}" id="exampleInputPassword1" placeholder="Password">
                                    @if($errors->has('password'))
                                            <small class="form-text invalid-feedback">{{ $errors->first('password') }}</small>
                                    @endif
                                </div>
                                <div class="form-footer">
                                    <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>