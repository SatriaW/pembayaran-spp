<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
	<meta charset="UTF-8">
	<link rel="icon" type="image/png" href="{{ asset('img/logosmkn4.png') }}">
	<title>Pembayaran SPP</title>
	<link href="{{ asset('css/dashboard.css') }}" rel="stylesheet" />
</head>

<body class="">
	<div class="page">
		<div class="flex-fill">
			<div class="header" style="border-bottom: 0px">
				<div class="container">
					<div class="d-flex">
						<a class="header-brand row" href="{{ route('web.index') }}">
							<img src="{{ asset('img/logosmkn4.png') }}" style="width: 45px;margin-right: 10px">
							<h3 class="mt-2" style="margin-bottom: 10px !important">Pembayaran SPP</h3>
						</a>
						<div class="d-flex order-lg-2 ml-auto">
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@csrf
									</form>	
									<a class="form-label" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										<i class="dropdown-icon fe fe-log-out"></i> Keluar
									</a>
						</div>
					</div>
				</div>
			</div>
			<!-- start navbar -->
			@include('shared.navbar')
			<div class="my-3 my-md-5">
				<div class="container">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</body>
</html>