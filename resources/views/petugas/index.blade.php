@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Petugas
        </h1>
        <div class="page-options d-flex">
            <div class="input-icon ml-2">
                
                <form action="{{ route('petugas.cari') }}" method="GET">
                    <span class="input-icon-addon">
                        <i class="fe fe-search"></i>
                    </span>
                    <input type="text" class="form-control w-10" placeholder="Cari nama petugas" name="cari">
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Petugas</h3>
                    <a href="{{ route('petugas.create') }}" class="btn btn-outline-primary btn-sm ml-5">Tambah Petugas</a>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-hover table-vcenter text-nowrap">
                        <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>Nama petugas</th>
                            <th>E-mail</th> 
                            <th>Action</th> 
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($petugas as $no => $p)
                            <tr>
                                <td><span class="text-muted">{{ ++$no + ($petugas->currentPage()-1) * $petugas->perPage() }}</span></td>
                                <td>{{ $p->nama_petugas }}</td>
                                <td>{{ $p->email }}</td>
                                <td>
                                    @if ( $p->nama_petugas != "Admin" )
                                    <a class="icon" href="petugas/{{ $p->nign }}/detail" title="lihat detail">
                                        <i class="fe fe-eye"></i> 
                                    </a>
                                    <a class="icon" href="petugas/{{ $p->nign }}/ubah" title="edit item">
                                        <i class="fe fe-edit"></i> 
                                    </a>
                                    <a class="icon btn-delete" href="petugas/{{ $p->nign }}/hapus" data-id="" title="delete item">
                                        <i class="fe fe-trash"></i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        {{ $petugas->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
