@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detail petugas</h3>
                </div>
                @foreach ($petugas as $p)
                <div class="card-body">
                    <p><b>NIGN</b> : {{$p->nign}}</p>
                    <p><b>Nama</b> : {{$p->nama_petugas}}</p>
                    <p><b>E-mail</b> : {{$p->email}}</p>
                    <p><b>Password</b> : {{$p->password}} </p>
                </div>
                @endforeach
            <div class="card-footer text-right">
                <div class="d-flex">
                    <a href="{{ url()->previous() }}" class="btn btn-link">Kembali</a>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection