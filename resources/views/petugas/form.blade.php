@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-8">
            <form action="{{ route('petugas.store') }}" method="post" class="card">
                {{ csrf_field() }}
                <div class="card-header">
                    <h3 class="card-title">Tambah petugas</h3>
                </div>
                <div class="card-body">
                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" name="role" class="form-control" value="Petugas">
                            <div class="form-group">
                                <label class="form-label">NIGN</label>
                                <input type="number" class="form-control" name="nign" placeholder="Masukkan NIGN" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nama Petugas</label>
                                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">E-mail</label>
                                <input type="email" class="form-control" name="email" placeholder="Masukkan E-mail" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Masukkan password" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        <a href="{{ url()->previous() }}" class="btn btn-link">Batal</a>
                        <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection