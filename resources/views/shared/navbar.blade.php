@if(Auth::user()->role == 'Admin')
<div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="{{ route('spp.index') }}" class="nav-link {{ set_active(['spp.*'], 'active') }}">
                            <i class="fe fe-repeat"></i> Transaksi SPP
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('siswa.index') }}" class="nav-link {{ set_active(['siswa.*'], 'active') }}">
                            <i class="fe fe-users"></i> Siswa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('petugas.index') }}" class="nav-link {{ set_active(['petugas.*'], 'active') }}">
                            <i class="fe fe-users"></i> Petugas
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endif