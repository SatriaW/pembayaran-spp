@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-8">
            @foreach($siswa as $s)
            <form action="/siswa/update" method="post" class="card">
                {{ csrf_field() }}
                <div class="card-header">
                    <h3 class="card-title">Tambah siswa</h3>
                </div>
                <div class="card-body">
                     @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                    @endif 
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <div class="form-group">
                                <label class="form-label">NIS</label>
                                <input type="number" class="form-control" placeholder="Masukkan NIS" value="{{ $s->nis }}" required disabled>
                                <input type="hidden" class="form-control" name="nis" placeholder="Masukkan NIS" value="{{ $s->nis }}" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nama peserta</label>
                                <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" value="{{ $s->nama }}" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Kelas</label>
                                <input type="text" class="form-control" name="kelas" placeholder="Contoh : XII RPL 2" value="{{ $s->kelas }}" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <textarea class="form-control" name="alamat" placeholder="Masukkan alamat" value="">{{ $s->alamat }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">E-mail</label>
                                <input type="email" class="form-control" name="email" placeholder="Masukkan E-mail" value="{{ $s->email }}" required>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Masukkan password" value="{{ $s->password }}" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        <a href="{{ url()->previous() }}" class="btn btn-link">Batal</a>
                        <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                    </div>
                </div>
            </form>
            @endforeach
        </div>
    </div>
@endsection