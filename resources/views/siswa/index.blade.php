@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Siswa
        </h1>
        <div class="page-options d-flex">
            <div class="input-icon ml-2">
                
                <form action="{{ route('siswa.cari') }}" method="GET">
                    <span class="input-icon-addon">
                        <i class="fe fe-search"></i>
                    </span>
                    <input type="text" class="form-control w-10" placeholder="Cari nama siswa" name="cari">
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Siswa</h3>
                    <a href="{{ route('siswa.create') }}" class="btn btn-outline-primary btn-sm ml-5">Tambah Siswa</a>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-hover table-vcenter text-nowrap">
                        <thead>
                        <tr>
                            <th class="w-1">No.</th>
                            <th>Nis</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Alamat</th>
                            <th>Action</th> 
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($siswa as $no => $s)
                            <tr>
                                <td><span class="text-muted">{{ ++$no + ($siswa->currentPage()-1) * $siswa->perPage() }}</span></td>
                                <td>{{ $s->nis }}</td>
                                <td>{{ $s->nama }}</td>
                                <td>{{ $s->kelas }}</td>
                                <td>{{ $s->alamat }}</td>
                                <td>
                                    <a class="icon" href="siswa/{{ $s->nis }}/detail" title="lihat detail">
                                        <i class="fe fe-eye"></i> 
                                    </a>
                                    <a class="icon" href="siswa/{{ $s->nis }}/ubah" title="edit item">
                                        <i class="fe fe-edit"></i> 
                                    </a>
                                    <a class="icon btn-delete" href="siswa/{{ $s->nis }}/hapus" data-id="" title="delete item">
                                        <i class="fe fe-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        {{ $siswa->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
