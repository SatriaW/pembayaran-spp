@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-8">
            <form action="" method="post" class="card">
                <div class="card-header">
                    <h3 class="card-title">Tambah siswa</h3>
                </div>
                <div class="card-body">
                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <input type="hidden" name="role" class="form-control" value="Siswa">
                            <div class="form-group">
                                <label class="form-label">NIS</label>
                                <input type="number" class="form-control" name="nis" placeholder="Masukkan NIS" required value="{{ old('nis') }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Nama siswa</label>
                                <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap" required value="{{ old('nama') }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Kelas</label>
                                <input type="text" class="form-control" name="kelas" placeholder="Contoh : XII RPL 2" required value="{{ old('kelas') }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <textarea class="form-control" name="alamat" placeholder="Masukkan alamat" >{{ old('nis') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">E-mail</label>
                                <input type="email" class="form-control" name="email" placeholder="Masukkan E-mail" required value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Masukkan password" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <div class="d-flex">
                        <a href="{{ url()->previous() }}" class="btn btn-link">Batal</a>
                        <button type="submit" class="btn btn-primary ml-auto">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection