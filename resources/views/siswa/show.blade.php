@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Detail siswa</h3>
                </div>
                @foreach ($siswa as $s)
                <div class="card-body">
                    <p><b>NIS</b> : {{$s->nis}}</p>
                    <p><b>Nama</b> : {{$s->nama}}</p>
                    <p><b>Kelas</b> : {{$s->kelas}}</p>
                    <p><b>Alamat</b> : {{$s->alamat}}</p>
                    <p><b>E-mail</b> : {{$s->email}}</p>
                    <p><b>Password</b> : {{$s->password}} </p>
                </div>
                @endforeach
            <div class="card-footer text-right">
                <div class="d-flex">
                    <a href="{{ url()->previous() }}" class="btn btn-link">Kembali</a>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection