<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaksi extends Model
{
    use SoftDeletes;

    protected $table = 'transaksi';

    protected $fillable = [
        'nis',
        'nign',
        'tgl_bayar',
        'bulan',
    ];

    function siswa(){
		return $this->belongsTo('App\Siswa','nis');
	}   
	function petugas(){
		return $this->belongsTo('App\Petugas','nign');
	}   
}
