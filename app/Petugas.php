<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Petugas extends Model
{
    use SoftDeletes;

    protected $table = 'petugas';

    protected $fillable = [
        'nama_petugas',
        'email',
        'password',
    ];

    protected $guarded = ['nign'];

    protected $primaryKey = "nign";
}
