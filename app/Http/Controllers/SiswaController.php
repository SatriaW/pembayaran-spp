<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Siswa;
use App\User;
use App\Transaksi;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class SiswaController extends Controller
{
    public function index(Request $request) 
    {
        $siswa = Siswa::orderBy('kelas','asc')->paginate(15);

        return view('siswa.index',['siswa' => $siswa]);
    }

    public function create()
    {
        return view('siswa.form');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nis' => 'required|numeric|digits:10|unique:siswa,nis',
            'nama' => 'required|string|max:255',
            'kelas' => 'required|max:9',
            'alamat' => 'required',
            'email' => 'required|email|unique:siswa,email',
            'password' => 'required|min:8',
        ]);

        $nis        = $request->Input('nis');
        $nama       = $request->Input('nama');
        $kelas      = $request->Input('kelas');
        $alamat     = $request->Input('alamat');
        $email      = $request->Input('email');
        $role       = $request->input('role');
        $now        = Carbon::now();
        $password   = $request->Input('password');
        $password1  = Hash::make($request->Input('password'));

        $data = array('nis' => $nis,
                'nama' => $nama,
                'kelas' => $kelas,
                'alamat' => $alamat,
                'email' => $email,
                'password' => $password,
                'created_at' => $now,
            );

        $data1 = array('nis' => $nis,
                'email' => $email,
                'role' => $role,
                'name' => $nama,
                'password' => $password1,
                'created_at' => $now,
            );

        Siswa::insert([$data]);
        User::insert([$data1]);

        return redirect('/siswa');
    }

    public function edit($siswa)
    {
        $siswa = Siswa::where('nis',$siswa)->get();

        return view('siswa.edit',['siswa' => $siswa]);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'nis' => 'required|numeric|digits:10',
            'nama' => 'required|string|max:255',
            'kelas' => 'required|max:9',
            'alamat' => 'required',
            'email' => 'required|email|unique:siswa,email',
            'password' => 'required|min:8',
        ]);
        
        $nis        = $request->Input('nis');
        $nama       = $request->Input('nama');
        $kelas      = $request->Input('kelas');
        $alamat     = $request->Input('alamat');
        $email      = $request->Input('email');
        $now        = Carbon::now();
        $password   = $request->Input('password');
        $password1  = Hash::make($request->Input('password'));

        Siswa::where('nis','=',$nis)->update([
            'nama' => $nama,
            'kelas' => $kelas,
            'alamat' => $alamat,
            'email' => $email,
            'updated_at' => $now,
            'password' => $password,
        ]);
        User::where('nis','=',$nis)->update([
            'email' => $email,
            'name' => $nama,
            'updated_at' => $now,
            'password' => $password1,
        ]);

        return redirect('/siswa');
    }

    public function destroy($siswa)
    {
        Siswa::where('nis','=',$siswa)->forceDelete();
        User::where('nis','=',$siswa)->forceDelete();
        Transaksi::where('nis','=',$siswa)->forceDelete();

        return redirect('/siswa');
    }

    public function search(Request $request)
    {
        $cari = $request->cari;

        $siswa = Siswa::where('nama','like',"%".$cari."%")->paginate(15);

        return view('siswa.index',['siswa' => $siswa]);
    }

    public function show($siswa)
    {
        $siswa = Siswa::where('nis',$siswa)->get();

        return view('siswa.show',['siswa' => $siswa]);
    }
}
