<?php

namespace App\Http\Controllers;

//use Request;
use Illuminate\Http\Request;
use App\Petugas;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class PetugasController extends Controller
{
    public function index(Request $request) 
    {
        $petugas = Petugas::orderBy('nama_petugas','asc')->paginate(15);

        return view('petugas.index',['petugas' => $petugas]);
    }

    public function create()
    {
    	return view('petugas.form');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
            'nign' => 'required|numeric|digits:9|unique:petugas,nign',
            'nama' => 'required|string|max:255',
            'email' => 'required|email|unique:siswa,email',
            'password' => 'required|min:8',
        ]);

    	$nign		= $request->Input('nign');
        $nama 		= $request->Input('nama');
        $email 		= $request->Input('email');
        $role		= $request->input('role');
        $now        = Carbon::now();
        $password 	= $request->Input('password');
        $password1 	= Hash::make($request->Input('password'));

        $data = array('nign' => $nign,
        		'nama_petugas' => $nama,
    			'email' => $email,
    			'password' => $password,
                'created_at' => $now,
            );

        $data1 = array('nign' => $nign,
                'email' => $email,
        		'role' => $role,
        		'name' => $nama,
    			'password' => $password1,
                'created_at' => $now,
            );

    	Petugas::insert([$data]);
    	User::insert([$data1]);

    	return redirect('/petugas');
    }

    public function edit($petugas)
    {
        $petugas = Petugas::where('nign',$petugas)->get();

        return view('petugas.edit',['petugas' => $petugas]);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'nign' => 'required|numeric|digits:9',
            'nama' => 'required|string|max:255',
            'email' => 'required|email|unique:siswa,email',
            'password' => 'required|min:8',
        ]);
        
        $nama       = $request->Input('nama');
        $email      = $request->Input('email');
        $now        = Carbon::now();
        $password   = $request->Input('password');
        $password1  = Hash::make($request->Input('password'));
        $nign       = $request->input('nign');

    Petugas::where('nign','=',$nign)->update([
        'nama_petugas' => $nama,
        'email' => $email,
        'updated_at' => $now,
        'password' => $password,
    ]);
    User::where('nign','=',$nign)->update([
        'email' => $email,
        'name' => $nama,
        'updated_at' => $now,
        'password' => $password1,
    ]);

    return redirect('/petugas');
    }

    public function destroy($petugas)
    {
        Petugas::where('nign','=',$petugas)->forceDelete();
        User::where('nign','=',$petugas)->forceDelete();

        return redirect('/petugas');
    }

    public function search(Request $request)
    {
        $cari = $request->cari;

        $petugas = Petugas::where('nama_petugas','like',"%".$cari."%")->paginate(15);

        return view('petugas.index',['petugas' => $petugas]);
    }

    public function show($petugas)
    {
        $petugas = Petugas::where('nign',$petugas)->get();

        return view('petugas.show',['petugas' => $petugas]);
    }
}
