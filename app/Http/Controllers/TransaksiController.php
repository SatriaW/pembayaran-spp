<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Siswa;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TransaksiController extends Controller
{
    public function index(Request $request)
    {
        $bulan = array('Januari' => 'Januari',
            'Februari' => 'Februari',
            'Maret' => 'Maret',
            'April' => 'April',
            'Mei' => 'Mei',
            'Juni' => 'Juni',
            'Juli' => 'Juli',
            'Agustus' => 'Agustus',
            'September' => 'September',
            'Oktober' => 'Oktober',
            'November' => 'November',
            'Desember' => 'Desember',
        );

            $nis = Auth::user()->nis;
            $siswa = Transaksi::where('nis','=',$nis)->orderBy('created_at','desc')->paginate(10);
            $transaksi = Transaksi::orderBy('created_at','desc')->paginate(10);

        return view('transaksi.index', [
            'siswa' => $siswa,
            'transaksi' => $transaksi,
            'bulan' => $bulan,
        ]);
    }

    //pay tagihan
    public function store(Request $request)
    {
        $this->validate($request,[
            'nis' => 'required|numeric|digits:10|exists:siswa,nis',
            'bulan' => 'required',
        ]);

        $nign = $request->input('id');
        $nis = $request->input('nis');
        $bulan = $request->input('bulan');
        $now        = Carbon::now();

        $data = array('nign' => $nign,
                'nis' => $nis,
                'bulan' => $bulan,
                'created_at' => $now,
                'tgl_bayar' => $now,
            );

        Transaksi::insert([$data]);

        return redirect('transaksi-spp');
    }

    public function destroy($transaksi)
    {
        Transaksi::where('id','=',$transaksi)->forceDelete();

        return redirect('/transaksi-spp');
    }

    public function edit($transaksi)
    {
        $transaksi = Transaksi::where('id',$transaksi)->get();
        $bulan = array('Januari' => 'Januari',
            'Februari' => 'Februari',
            'Maret' => 'Maret',
            'April' => 'April',
            'Mei' => 'Mei',
            'Juni' => 'Juni',
            'Juli' => 'Juli',
            'Agustus' => 'Agustus',
            'September' => 'September',
            'Oktober' => 'Oktober',
            'November' => 'November',
            'Desember' => 'Desember',
        );

        return view('transaksi.edit',['transaksi' => $transaksi,'bulan' => $bulan]);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'nis' => 'required|numeric|digits:10|exists:siswa,nis',
            'nign' => 'required|numeric|digits:9|exists:petugas,nign',
            'bulan' => 'required',
        ]);

        $id = $request->input('id');
        $nign = $request->input('nign');
        $nis = $request->input('nis');
        $bulan = $request->input('bulan');
        $now   = Carbon::now();

        Transaksi::where('id','=',$id)->update([
            'nign' => $nign,
            'nis' => $nis,
            'bulan' => $bulan,
            'updated_at' => $now,
        ]);

        return redirect('/transaksi-spp');
    }
}
