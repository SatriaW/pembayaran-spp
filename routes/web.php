<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('spp')->group(function(){
    Auth::routes();

    Route::middleware(['auth:web'])->group(function(){
        Route::get('/', 'TransaksiController@index')->name('web.index');

        //Siswa
        Route::get('siswa','SiswaController@index')->name('siswa.index');
        Route::get('tambah-siswa','SiswaController@create')->name('siswa.create');
        Route::post('tambah-siswa', 'SiswaController@store')->name('siswa.store');
        Route::get('siswa/{siswa}/ubah', 'SiswaController@edit')->name('siswa.edit');
        Route::post('siswa/update','SiswaController@update')->name('siswa.update');
        Route::get('siswa/{siswa}/hapus', 'SiswaController@destroy')->name('siswa.destroy');
        Route::get('caris', 'SiswaController@search')->name('siswa.cari');
        Route::get('siswa/{siswa}/detail', 'SiswaController@show')->name('siswa.show');

        //Petugas
        Route::get('petugas','PetugasController@index')->name('petugas.index');
        Route::get('tambah-petugas','PetugasController@create')->name('petugas.create');
        Route::post('tambah-petugas', 'PetugasController@store')->name('petugas.store');
        Route::get('petugas/{petugas}/ubah', 'PetugasController@edit')->name('petugas.edit');
        Route::post('petugas/update','PetugasController@update')->name('petugas.update');
        Route::get('petugas/{petugas}/hapus', 'PetugasController@destroy')->name('petugas.destroy');
        Route::get('carip', 'PetugasController@search')->name('petugas.cari');
        Route::get('petugas/{petugas}/detail', 'PetugasController@show')->name('petugas.show');

        //Pembayaran SPP
        Route::get('transaksi-spp','TransaksiController@index')->name('spp.index');
        Route::post('transaksi-spp/simpan', 'TransaksiController@store')->name('spp.store');
        Route::get('transaksi/{transaksi}/ubah', 'TransaksiController@edit')->name('transaksi.edit');
        Route::post('transaksi/update','TransaksiController@update')->name('transaksi.update');
        Route::get('transaksi/{transaksi}/hapus', 'TransaksiController@destroy')->name('transaksi.destroy');
    });




