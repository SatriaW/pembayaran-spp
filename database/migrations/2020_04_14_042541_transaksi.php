<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tgl_bayar');
            $table->string('bulan');
            $table->string('nign')->nullable();
            $table->string('nis');
            $table->timestamps();
            $table->softDeletes();

            
        });

        Schema::table('transaksi', function (Blueprint $table) {
            $table->foreign('nign')->references('nign')->on('petugas')->onDelete('cascade');
            $table->foreign('nis')->references('nis')->on('siswa')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
