<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('role' , ['Admin','Petugas','Siswa']);
            $table->string('nis')->nullable();
            $table->string('nign')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            //$table->foreign('id_petugas')->references('id')->on('petugas');
            //$table->foreign('nis')->references('nis')->on('siswa');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('nis')->references('nis')->on('siswa')->onDelete('cascade');
            $table->foreign('nign')->references('nign')->on('petugas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
