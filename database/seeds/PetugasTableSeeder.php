<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PetugasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('petugas')->insert([
            'nign' => 100000000,
            'nama_petugas' => 'Admin',
        ]);
    }
}
